<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  targetNamespace="http://www.cfs++.org/material"
  xmlns="http://www.cfs++.org/material"
  elementFormDefault="qualified">

  <xsd:annotation>
    <xsd:documentation xml:lang="en">
      Coupled Field Solver project CFS++
      Schema for material description of an magnetic material
    </xsd:documentation>
  </xsd:annotation>

  <xsd:include schemaLocation="CFS_MAThysteresis.xsd"/>

  <!-- ******************************************************************* -->
  <!--   Definition of material for magnetic PDEs -->
  <!-- ******************************************************************* -->

  <xsd:complexType name="DT_MatMagnetic">
    <xsd:sequence>
      <xsd:element name="electricConductivity" type="DT_ElecConductivity"/>
      <xsd:element name="permeability" type="DT_MagnPermeability" minOccurs="1" maxOccurs="unbounded"/>
      <xsd:element name="reluctivity_MagStrict" type="DT_MagstrictReluctivity" minOccurs="0"/>
      <xsd:element name="magnetoStrictionTensor_h_mag" type="DT_TensorGeneralType" minOccurs="0"/>
      <xsd:element name="hystModel" type="DT_HystModelMag" minOccurs="0"/>
	    <xsd:element name="prescribedMagnetization" type="DT_fixedMagnetization" minOccurs="0"/>
      <xsd:element name="density" type="DT_MagneticScalarLinear" minOccurs="0"/>
      <xsd:element name="coreLoss" type="DT_CoreLoss" minOccurs="0"/>
      <xsd:element name="permittivity" type="DT_MagnPermittivity" minOccurs="0"/>
    </xsd:sequence>
  </xsd:complexType>
  
  <xsd:complexType name="DT_MagneticScalarLinear">
    <xsd:sequence>
      <xsd:element name="linear" type="DT_ScalarType" minOccurs="0"/>
    </xsd:sequence>
  </xsd:complexType>
  
<!-- ******************************************************************* -->
  <!--   Definition of prescribed FIXED magnetization M (B = mu_0(H + M))  -->
  <!-- ******************************************************************* -->
  <xsd:complexType name="DT_fixedMagnetization">
    <xsd:annotation>
      <xsd:documentation>
        Prescribe Magnetization VECTOR in CARTESIAN coordinates (x,y,z) of material.
        Useable for permanent magnets.
        Cannot be combined with a hysteresis model.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:all>
      <xsd:element name="MagnetizationVector">
        <xsd:simpleType>
          <xsd:restriction base="DT_DoubleList">
            <xsd:minLength value="3"/>
            <xsd:maxLength value="3"/>
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  
  <!-- ******************************************************************* -->
  <!--   Definition of magnetic permeability-->
  <!-- ******************************************************************* -->
  <xsd:complexType name="DT_MagnPermeability">
    <xsd:sequence>
      <xsd:element name="linear" type="DT_Square3TensorType"/>
      <xsd:element name="nonlinear" type="DT_NonLinMagnPermeability" minOccurs="0"/>
      <xsd:element name="model" type="DT_ElecPermeabilityHyst" minOccurs="0"/>
    </xsd:sequence>
    <xsd:attribute name="temperature" type="xsd:float" use="optional">
      <xsd:annotation>
        <xsd:documentation>
          This attribute is only to be used if there are several BH-curves for different
          temperatures (temperature-dependent BH-curves)
        </xsd:documentation>
      </xsd:annotation>
    </xsd:attribute>
  </xsd:complexType>
  

  <xsd:complexType name="DT_ElecPermeabilityHyst">
    <xsd:all>
      <xsd:element name="isotropic" minOccurs="1">
        <xsd:complexType>
          <xsd:choice>
            <xsd:element name="JilesAthertonModel" type="DT_JilesAthertonModel"/>
            <xsd:element name="EBHysteresisModel" type="DT_EBHysteresisModel"/>
          </xsd:choice>
        </xsd:complexType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>


  <!-- ******************************************************************* -->
  <!--   Definition of permittivity-->
  <!-- ******************************************************************* -->
  <xsd:complexType name="DT_MagnPermittivity">
    <xsd:sequence>
      <xsd:element name="linear" type="DT_Square3TensorType"/>
    </xsd:sequence>
  </xsd:complexType>
  
  
  <!-- ******************************************************************* -->
  <!--   Definition of non linear magnetic permeability-->
  <!-- ******************************************************************* -->
  <xsd:complexType name="DT_NonLinMagnPermeability">
    <xsd:choice>
      <xsd:element name="isotropic" type="DT_IsotrNonLinMagnPermeability"/>
      <xsd:element name="anisotropic" type="DT_AnisotrNonLinMagnPermeability"/>
    </xsd:choice>
  </xsd:complexType>

  <!-- ******************************************************************* -->
  <!--   Definition of isotropic non linear magnetic permeability-->
  <!-- ******************************************************************* -->
  <xsd:complexType name="DT_IsotrNonLinMagnPermeability">
    <xsd:complexContent>
      <xsd:extension base="DT_MatNonLinearity">
        <!-- A choice is included here because we are only allowed to define mu OR nu -->
      <xsd:choice>
        <xsd:sequence>
          <xsd:element name="muExpr" type="xsd:string" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
              <xsd:documentation xml:lang="en">
                Only valid when H-based formulations (Psi) in magnetic PDE are used!
                This value is only necessary for the analytic approxType. It is a string interpreted by the math parser, where "H_R" represents
                the Euclidean norm of the magnetic field strength. Basically, any function depending on H can be specified.
              </xsd:documentation>
            </xsd:annotation>
          </xsd:element>
          <xsd:element name="muDerivExpr" type="xsd:string" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
              <xsd:documentation xml:lang="en">
                Only valid when H-based formulations (Psi) in magnetic PDE are used!
                This value is only necessary for the analytic approxType. It is a string interpreted by the math parser, where "H_R" represents
                the Euclidean norm of the magnetic field strength. It represents the first derivative of muExpr with respect to H.
              </xsd:documentation>
            </xsd:annotation>
          </xsd:element>
        </xsd:sequence>
        <xsd:sequence>
          <xsd:element name="nuExpr" type="xsd:string" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
              <xsd:documentation xml:lang="en">
                Only valid when B-based formulations (A, A-V, specialAV, Darwin, Darwin_doubleLagrange) in magnetic or magneticEdge PDE are used!
                This value is only necessary for the analytic approxType. It is a string interpreted by the math parser, where "B_R" represents
                the Euclidean norm of the flux density. Basically, any function depending on B can be specified.
              </xsd:documentation>
            </xsd:annotation>
          </xsd:element>
          <xsd:element name="nuDerivExpr" type="xsd:string" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
              <xsd:documentation xml:lang="en">
                Only valid when B-based formulations (A, A-V, specialAV, Darwin, Darwin_doubleLagrange) in magnetic or magneticEdge PDE are used!
                This value is only necessary for the analytic approxType. It is a string interpreted by the math parser, where "B_R" represents
                the Euclidean norm of the flux density. It represents the first derivative of nuExpr with respect to B.
              </xsd:documentation>
            </xsd:annotation>
          </xsd:element>
        </xsd:sequence>  
      </xsd:choice>  
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>
  
  <!-- ******************************************************************* -->
  <!--   Definition of non linear BH curves (ansiotropic)-->
  <!-- ******************************************************************* -->
  <xsd:complexType name="DT_AnisotrNonLinMagnPermeability">
    <xsd:sequence>
      <xsd:element name="data" type="DT_AnisotropicNonLinBHCurves" minOccurs="0" maxOccurs="unbounded"/>
    </xsd:sequence>
  </xsd:complexType>
  
  <xsd:complexType name="DT_AnisotropicNonLinBHCurves">
    <xsd:complexContent>
      <xsd:extension base="DT_IsotrNonLinMagnPermeability">
        <xsd:sequence>
          <xsd:element name="angle" type="DT_NonNegFloat"/>
          <xsd:element name="zScaling" type="DT_NonNegFloat"/>
        </xsd:sequence>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>  



  <!-- ******************************************************************* -->
  <!--   Definition of nonlinear magnetic reluctivity (only for magstrict coupling) -->
  <!-- ******************************************************************* -->
  <xsd:complexType name="DT_MagstrictReluctivity" >
    <xsd:sequence>
      <xsd:element name="nonlinear" type="DT_NonLinMagstrictReluctivity" minOccurs="1"/>
    </xsd:sequence>
  </xsd:complexType>  

  <!-- ******************************************************************* -->
  <!--   Definition of non linear magnetic reluctivity -->
  <!-- ******************************************************************* -->
  <xsd:complexType name="DT_NonLinMagstrictReluctivity">
    <xsd:choice>
      <xsd:element name="isotropic" type="DT_MatNonLinearity"/>
    </xsd:choice>
  </xsd:complexType>

  <!-- ******************************************************************* -->
  <!--   Information on coil loss -->
  <!-- ******************************************************************* -->
  <xsd:complexType name="DT_CoreLoss">
    <xsd:sequence>
      <xsd:element name="dataName" type="xsd:string"/>
    </xsd:sequence>
  </xsd:complexType>

</xsd:schema>
