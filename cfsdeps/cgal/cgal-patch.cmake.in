# -*- mode: cmake; coding: utf-8; indent-tabs-mode: nil; -*-
# kate: indent-width 2; encoding utf-8; auto-brackets on;
# kate: mixedindent on; indent-mode cstyle; line-numbers on;
# kate: syntax cmake; replace-tabs on; background-color #D1EBFF;
# kate: remove-trailing-space on; bracket-highlight-color #ff00ff;

#=============================================================================
# Set global variables, configured from the external build.
#=============================================================================
set(cgal_source  "@cgal_source@")
set(cgal_prefix  "@cgal_prefix@")
SET(CFS_SOURCE_DIR "@CFS_SOURCE_DIR@")
SET(CFS_BINARY_DIR "@CFS_BINARY_DIR@")
SET(CMAKE_CXX_COMPILER_ID "@CMAKE_CXX_COMPILER_ID@")

#=============================================================================
# Include some convenient macros (e.g. for applying patches).
#=============================================================================
INCLUDE("${CFS_SOURCE_DIR}/cmake_modules/CFS_macros.cmake")

EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND}
  -E remove_directory
  "${CFS_BINARY_DIR}/include/CGAL"
)

EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND}
  -E copy_if_different
  "${cgal_prefix}/CGAL_SetupBoost.cmake"
  "${cgal_source}/cmake/modules/CGAL_SetupBoost.cmake"
)

#=============================================================================
# Apply some patches.
#=============================================================================
SET(patches
  # CGAL: Syntax Errors on intmax_t and uintmax_t
  # http://choorucode.com/2010/08/03/cgal-syntax-errors-on-intmax_t-and-uintmax_t/
  # http://www.mpfr.org/mpfr-2.4.2/#stdint
  "cgal-intmax_t.patch"
  )

  if(CMAKE_CXX_COMPILER_ID MATCHES "Intel")
  list(APPEND patches "cgal-intel-isfinite.patch")
endif()

APPLY_PATCHES("${patches}" "${CFS_SOURCE_DIR}/cfsdeps/cgal")
